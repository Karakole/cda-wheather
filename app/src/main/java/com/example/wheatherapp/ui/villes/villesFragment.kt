package com.example.wheatherapp.ui.villes

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.wheatherapp.R
import com.example.wheatherapp.global.Connectivity

class villesFragment : Fragment() {

    companion object {
        fun newInstance() = villesFragment()
    }

    private lateinit var viewModel: VillesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.villes_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(VillesViewModel::class.java)
       Connectivity.getVilles(requireContext())
    }

}