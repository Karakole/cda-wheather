package com.example.wheatherapp.global

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.wheatherapp.R
import org.json.JSONObject

object Connectivity {
    val urlGouv = Url.gouvUrl + "departements/64/communes"

    fun getVilles(context:Context){
        val queue= Volley.newRequestQueue(context)
        val jsonArrayRequest  = JsonArrayRequest(urlGouv,
        {
            response ->
            Log.d("Data===>", response.toString())
        },

            { error ->
                Log.d("Error==>", error.toString())
            }
        )
        queue.add(jsonArrayRequest)
    }


    fun getVille(context:Context, view: View){
        val urlApiMeteo = Url.url + "forecast/daily/0?token=" + Url.token + "&insee=64445"
        val queue= Volley.newRequestQueue(context)
        val jsonObjectRequest  = JsonObjectRequest(urlApiMeteo,
            {
                    response ->
                Log.d("forecast===>", response.getJSONObject("forecast").toString())
                Log.d("Data===>", response.toString())
                val ville = response.getJSONObject("city")
                val forecast = response.getJSONObject("forecast")
                view.findViewById<TextView>(R.id.villeName).text = ville.getString("name")
                view.findViewById<TextView>(R.id.rain).text = forecast.getInt("probarain").toString() + "%"
                view.findViewById<TextView>(R.id.temperatureMin).text = forecast.getInt("tmin").toString() + "°C"
                view.findViewById<TextView>(R.id.temperatureMax).text = forecast.getInt("tmax").toString() + "°C"
                val dateTime = forecast.getString("datetime").substring(0, 10)
                view.findViewById<TextView>(R.id.date).text = dateTime
            },

            { error ->
                Log.d("Error==>", error.toString())
            }
        )
        queue.add(jsonObjectRequest)
    }
}